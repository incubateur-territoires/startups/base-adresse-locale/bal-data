# bal-data

Ce dépôt regroupe les briques essentielles à la constitution de données utiles à l'études des Bases Adresses Locales

C'est à dire :
1 - l'acquisition
2 - le traitement
3 - l'enrichissement
4 - la production de fichiers consolidés

## L'acquisition
- commune-summary.csv :  l'état de la Base Adresse Nationale par commune.
- ban-csv : l'intégralité des données de la BAN
- bal-csv : l'intégralité des données BAL dans la BAN
- api-bal : Les métadonnées des BAL Mes Adresses
- api-depot : Les métadonnées des BAL déposés via l'API de dépot
- annuaire : Les informations des communes dans l'annuaire du service public.
- api-geo : Les informations des communes dans l'API géo.
- api-decoupage-administratif : les informations des communes sur l'API découpage administratif
- api-zammad : Les informations du support adresse (nécessite une clé d'API privée)
- backend : Les informations des communes de BAL déposés via moissonage et dépot sur data.gouv.fr (décommissionée depuis le moissonneur V2)


## Le traitement
- Corrections diverses sur les sources
- Aggrégation des métadonnées sur chaques communes
- Consolidation de l'ensemble des fichiers

# Production de fichiers consolidés
- Ficiers de sortis en .pkl pour utilisation avec python+pandas.
