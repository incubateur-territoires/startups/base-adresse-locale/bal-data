from setuptools import setup, find_packages

with open('README.md', 'r') as readme_file:
    long_description = readme_file.read()

setup(
      name = 'bal-data',
      version = '0.1',
      description = """"Downloads and parse BAN data
                    for statistics purposes""",
      long_description=long_description,
      long_description_content_type = 'text/markdown',
      author = 'Jules Saur',
      author_email = 'jules.saur@beta.gouv.fr',
      licence='MIT',
      classifiers=[
          "Development Status :: 4 - Beta"
          "Licence :: OSI Aproved :: MIT Licence"
          ],
      keywords='Base Adresse Locale',
      packages = find_packages(), #exclude=('data')
      install_requires=['requests', 'pandas'],
      python_requires='3.9'
) 
