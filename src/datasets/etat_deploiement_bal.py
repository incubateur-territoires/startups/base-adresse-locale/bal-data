#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul  1 14:35:44 2022

@author: panda
"""

import pandas as pd
from src.traitement.utils import get_fresh_pickle

#here should be optimization as well
# Set typeEpci to categorical data
# epci_types = pd.api.types.CategoricalDtype(categories=['CC', 'CU', 'ME','CA','CI'],ordered=False)
# df['EPCI_TYPE'] = df['EPCI_TYPE'].astype(epci_types)
#
# TURN STUFF TO INT 32 WHEN POSSIBLE
#

pickle = pd.read_pickle(get_fresh_pickle('etat_deploiement_bal'))
