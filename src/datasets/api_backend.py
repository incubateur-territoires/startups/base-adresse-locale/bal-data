#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul  5 16:23:39 2022

@author: panda
"""

import pandas as pd
from src.traitement.utils import get_fresh_pickle

#here should be optimization as well
pickle = pd.read_pickle(get_fresh_pickle('backend'))
