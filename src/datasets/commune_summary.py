#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul  1 14:36:16 2022

@author: panda
"""
import pandas as pd
from src.traitement.utils import get_fresh_pickle

commune_summary = pd.read_pickle(get_fresh_pickle('commune_summary'))