#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 27 23:52:11 2021
@author: cactus
src 1
"""
# ---------------
#     IMPORTS
# ---------------
import os
from dotenv import load_dotenv
from datetime import datetime
import json
import requests
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
import logging
from bs4 import BeautifulSoup
import pandas as pd



# to give api_depot access to code insee liste
from src.traitement.utils import get_fresh_csv 

# loading environment variable : zammad API Key and path to data folder
load_dotenv()


# ---------------------------------------------------------------------
# FONCTIONS : Récupération de JSON sur les APIS adresse.data et geo.api
# ----------------------------------------------------------------------
def request_json_data(url):
    """
    Return JSON object from an API endpoint.
    """
    # Use Requests to download from URL
    r = requests.get(url)
    # if data is received, return a JSON object
    if r.status_code == requests.codes.ok:
        return r.json()
    else:
        print('ERROR : '+str(r.status_code))


def dump_json(js,js_name, folder_name='', datify_js_name=True):
    """
    Write a JSON to a file in folder /data/json/ with the date
    
    """    
    # Logging
    logging.basicConfig(level=logging.DEBUG)
    
    # Get today's date
    if datify_js_name == True:
        dt_string = '_'+datetime.now().strftime("%Y_%m_%d_%H%M")
    else:
        dt_string = ''
    
    # Create folder if none existent
    if len(folder_name)==0:
        path= os.getenv('DATA_PATH') + 'json/'
    else :
        path = os.getenv('DATA_PATH') + 'json/'+folder_name+'/'
    if not os.path.exists(path):
        os.makedirs(path)
        
    # Write the json to file : /folder_name/json_name+date+.json
    with open(path+js_name+dt_string+'.json', "w") as outfile:
        json.dump(js, outfile)
        logging.debug('Wrote '+path+js_name+dt_string+'.json')

# ----------------------
# Query Commune-Summary
# ----------------------
def query_commune_summary():
    """
    Download a csv file from data.gouv.fr 
    containing commune addresses information
    and save it in /data/csv/
    """   
    # Get today's date
    dt_string = datetime.now().strftime("%Y_%m_%d_%H%M")
    # Create folder if none existent
    path = os.getenv('DATA_PATH')+'csv/'
    if not os.path.exists(path):
        os.makedirs(path)
    stable_url = "https://www.data.gouv.fr/fr/datasets/r/be62035b-c5ea-4f90-ac65-59df9cf70947"
    df = pd.read_csv(stable_url, low_memory=False)
    df.to_csv(path+'commune_summary_'+dt_string+'.csv', index=False)
    print('commune_summary_'+dt_string+'.csv'+' written to '+path)


# ----------------
# Query BAN
# ----------------
def query_ban_csv(date='latest'):
    """
    Write csv.gz files to /data/gz/ with all BAN adresses and lieux-dits
    """
    logging.basicConfig(level=logging.DEBUG)
    
    url = 'https://adresse.data.gouv.fr/data/ban/adresses/{}/csv/'.format(date)

    s = requests.Session()
    retries = Retry(total=10, backoff_factor=1, status_forcelist=[502, 503, 504])
    s.mount('https://', HTTPAdapter(max_retries=retries))

    s = requests.get(url)
    soup = BeautifulSoup(s.text, 'html.parser')
    links = []
    for link in soup.find_all('a'):
        links.append(link.get('href'))
    links.remove('adresses-france.csv.gz')
    links.remove('../')
    path = os.getenv('DATA_PATH')+'gz/ban/'+date+'/'
    if not os.path.exists(path):
        os.makedirs(path)

    for link in links:
        s = requests.get(url+link)
        with open(path+link, 'wb') as f:
            f.write(s.content) 



# ----------------
# Query BAL_CSV
# ----------------
def query_bal_csv(date='latest'):
    """
    Write csv.gz files to /data/gz/ with all BAL adresses
    """
    logging.basicConfig(level=logging.DEBUG)

    url = 'https://adresse.data.gouv.fr/data/adresses-locales/{}/csv/'.format(date)

    s = requests.Session()
    retries = Retry(total=10, backoff_factor=1, status_forcelist=[ 502, 503, 504 ])
    s.mount('https://', HTTPAdapter(max_retries=retries))

    s = requests.get(url)
    soup = BeautifulSoup(s.text, 'html.parser')
    links = []
    for link in soup.find_all('a'):
        links.append(link.get('href'))
    links.remove('../')
    path = os.getenv('DATA_PATH')+'gz/bal/'+date+'/'
    if not os.path.exists(path):
        os.makedirs(path)

    for link in links:
        s = requests.get(url+link)
        with open(path+link, 'wb') as f:
            f.write(s.content) 


# -----------------------
# Query Mes Adresses API
# -----------------------
def query_api_bal(api_url='https://api-bal.adresse.data.gouv.fr/v1/bases-locales'):
    """
    Write Mes Adresses API endpoint /bases-locales to json file in /data/json/api-bal_[date].json

    """
    # Logging
    # logging.basicConfig(level=logging.ERROR)  # filename='./logs/lumberjack.log')

    # folder /data/
    folder_name = ''
    # json file name
    js_name = 'api_bal'

    # Setting up Requests
    s = requests.Session()
    retries = Retry(total=100, backoff_factor=1, status_forcelist=[502, 503, 504])
    s.mount('https://', HTTPAdapter(max_retries=retries))

    # Sending request to the api
    r = s.get(api_url)
    js = r.json()
    # Writting the answer in a json file /data/json/api-bal_date.json
    dump_json(js=js, js_name=js_name, folder_name=folder_name)


# -------------------
# Query API de dépot
# -------------------
def query_api_depot(codes_insee= pd.read_csv(get_fresh_csv('commune_summary'))['code_commune'],
                    api_url='https://plateforme.adresse.data.gouv.fr/api-depot',
                    folder_name='api_depot'):
    """
    Iterate code_insee on /communes/{code_insee}/revisions
    MAYBE USE https://plateforme.adresse.data.gouv.fr/api-depot/current-revisions first! to get INSEE list
    """
    # Logging
    # logging.basicConfig(level=logging.ERROR)  # filename='./logs/lumberjack.log')

    # Setting up Requests
    s = requests.Session()
    retries = Retry(total=100, backoff_factor=1, status_forcelist=[502, 503, 504])
    s.mount('https://', HTTPAdapter(max_retries=retries))

    for code in codes_insee:
        r = s.get(api_url + '/communes/{0}/revisions'.format(code))
        js = r.json()
        if len(js) > 0:
            dump_json(js=js, js_name='api_depot_{}'.format(code), folder_name=folder_name, datify_js_name=False)


# ----------------
# Query Annuaire
# ----------------
def query_annuaire():
    """
    Write JSON file to /data/json/ with all communes contacts
    """
    logging.basicConfig(level=logging.DEBUG) #,filename='./logs/lumberjack.log')
    # Get data from the API
    url = "https://etablissements-publics.api.gouv.fr/v3/organismes/mairie"
    r = request_json_data(url)
    # Write JSON to a file
    dump_json(r,'annuaire')

# ------------------------------
# Query Decoupage Administratif
# ------------------------------
def query_decoupage_administratif():
    """
    Write JSON file to /data/json/ with all Communes metadatas
    """
    logging.basicConfig(level=logging.DEBUG)
    # Get data from the API
    url_decoupage_administratif = 'https://geo.api.gouv.fr/communes'
    r = request_json_data(url_decoupage_administratif)
    # Write JSON to a file
    dump_json(r,'decoupage_administratif')



# Require ADM
def query_geo_data_communes(df):
    """
    Iterate adm DataFrame and fetch additional data from geo.api.gouv.fr :
     - surface: float, en m2
     - contour: list polygon coordonnées lat lon
     - center:  list coordonnées lat lon
    /!\ Takes aprox. 35000 seconds to run because of API query limitations.
    """
    logging.basicConfig(level=logging.DEBUG)  # filename='./logs/lumberjack.log')
    s = requests.Session()
    retries = Retry(total=100, backoff_factor=1, status_forcelist=[502, 503, 504])
    s.mount('https://', HTTPAdapter(max_retries=retries))

    df['contour'] = pd.Series(dtype=object)
    df['centre'] = pd.Series(dtype=object)

    for i, row in df.iterrows():
        try:
            code_commune_INSEE = df.at[i, 'code']
            r = s.get('https://geo.api.gouv.fr/communes?code={0}&fields=code,nom,surface,centre,contour'.format(
                code_commune_INSEE))
            print(r.status_code)
            r = r.json()
            df.at[i, 'surface'] = r[0]['surface']
            df.at[i, 'contour_type'] = r[0]['contour']['type']
            df.at[i, 'contour'] = r[0]['contour']['coordinates']
            df.at[i, 'centre'] = r[0]['centre']['coordinates']
        except:
            print('ERROR')
            print(df.at[i, 'code'])
    return df

# Require ADM
def query_geo_names(df):
    """
    nom region
    nom derpartement

    """
    r = requests.get('https://geo.api.gouv.fr/regions')
    for region in r.json():
        df.loc[df.codeRegion == region['code'], 'nomRegion'] = region['nom']
    for code_region in df.codeRegion.value_counts().index:
        r = requests.get('https://geo.api.gouv.fr/regions/{}/departements'.format(str(code_region)))
        for departement in r.json():
            df.loc[df.codeDepartement == departement['code'], 'nomDepartement'] = departement['nom']
    return df



# ------------------
# Query Api Backend
# ------------------
def query_api_backend():
    """
    Write JSON file to /data/json/ with all BAL datasets metadatas.
    """
    logging.basicConfig(level=logging.DEBUG) #,filename='./logs/lumberjack.log')
    # Get data from the API
    url = 'https://backend.adresse.data.gouv.fr/datasets'
    r = request_json_data(url)
    # Write JSON to a file
    dump_json(js=r,js_name='api-backend')


# ----------------
# Query Zammad
# ----------------
def query_zammad():
    """
    Write JSON files to /data/json/ with all pages of Zammad tickets
    """
    logging.basicConfig(level=logging.DEBUG)
    s = requests.Session()
    # Using Token generated in web UI with jules.saur@anct.gouv.fr
    
    s.headers['Authorization'] = 'Token token='+ os.getenv('ZAMMAD_API_KEY')
    base = 'https://etalab-support.zammad.com/api/v1/tickets?expand=true&'
    page = 1
    proceed = True
    while proceed == True:
        query = 'page={0}&per_page=100'.format(page)
        js = s.get(base + query).json()
        if len(js) == 0:
            proceed = False
        else:
            if page < 10:
                page_num = '0' + str(page)
            else:
                page_num = str(page)
            dump_json(js=js, js_name='zammad_{0}'.format(page_num), folder_name='zammad', datify_js_name=False)
            page += 1


# -----
# MAIN
# -----
if __name__ == "__main__":
    print('Acquisition Script')
    # query_bal()
    # query_mes_adresses()
    # query_ban_csv()
    # query_zammad()
    # query_commune_summary()
    #query_decoupage_administratif()





# def query_mes_adresses():
#     """
#     Write JSON file to /data/json/ with all BAL datasets metadatas published with "Mes Adresses".
#     """
#     logging.basicConfig(level=logging.DEBUG) #,filename='./logs/lumberjack.log')
#     # Get data from the API
#     url = 'https://backend.adresse.data.gouv.fr/publication/submissions/published'
#     r = request_json_data(url)
#     # Write JSON to a file
#     dump_json(r,'mes_adresses')
    
    
# def query_bal():
#     """
#     Write JSON file to /data/json/ with all BAL datasets metadatas.
#     """
#     logging.basicConfig(level=logging.DEBUG) #,filename='./logs/lumberjack.log')
#     # Get data from the API
#     url = 'https://backend.adresse.data.gouv.fr/datasets'
#     r = request_json_data(url)
#     # Write JSON to a file
#     dump_json(r,'bal')
    

