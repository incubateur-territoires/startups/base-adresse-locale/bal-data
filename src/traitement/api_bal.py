import pandas as pd
from src.traitement.utils import load_json_from_file, get_fresh_json, dump_pickle


def parse_api_bal(filename='api_bal', as_file=True, as_frame=False):
    """
    Load raw Mes Adresses DataFrame and return it with correct dtypes and columns names.
    """

    df = pd.json_normalize(load_json_from_file(get_fresh_json(filename)))
    df.rename(columns={'sync.status':'sync_status',
                       'sync.isPaused':'sync_isPaused',
                       'sync.currentUpdated':'sync_currentUpdated',
                       'sync.lastUploadedRevisionId':'sync_lastUploadedRevisionId' },
              inplace=True)
    # Generation
    if as_file==True:
        dump_pickle(df, filename)
    if as_frame==True:
        return df        
    print("Parsing api-bal completed.")
    