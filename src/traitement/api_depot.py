# TODO Clean import and test from file (for now its copy paste from jupyter)

import os
from dotenv import load_dotenv
# loading environment variable :  path to data folder
load_dotenv()
import pandas as pd

from src.traitement.utils import read_simple_json, load_json_from_file, dump_pickle


def parse_api_depot(filename='api_depot', path=os.getenv('DATA_PATH')+'json/api_depot/', as_file=True, as_frame=False):
    print(path)
    files = []
    for file in os.listdir(path):
        if filename in file:
            files.append(file)
    files.sort(reverse=False)
    print(files)

    # df = read_simple_json(load_json_from_file(files[0], path=path))

    pages = []
    for file in files:
        page = read_simple_json(load_json_from_file(file, path=path))
        pages.append(page)
    df = pd.concat(pages)
    columns = dict(zip(df.columns, df.columns.str.replace('.', '_', regex=False)))
    df.rename(columns=columns, inplace=True)
    # Generation
    if as_file==True:
        dump_pickle(df, filename)
    if as_frame==True:
        return df        
    print("Parsing Api Dépôt completed.")
