import os
from dotenv import load_dotenv
# loading environment variable :  path to data folder
load_dotenv()


import sys
# adding src folder into path
root_dir = os.path.join(os.getcwd(), '..')
sys.path.append(root_dir)


import re
from datetime import datetime
import json

import logging
import pandas as pd
import gzip





# -----------------------------------------------------------------------
# FONCTIONS : Traitement des JSON en local pour compléter les DataFrames
# -----------------------------------------------------------------------

#--------
#  JSON
#--------
def load_json_from_file(filename, path=os.getenv('DATA_PATH')+'json/'):
    """
    Given a JSON file name from /data/json/, return it into a JSON object
    """
    
    f = open(path+filename)
    js = json.load(f)
    return js

def read_simple_json(js):
    """
    Load a JSON object into a DataFrame
    """
    df = pd.json_normalize(js)
    return df

def get_fresh_json(filename):
    """
    Return name of latest json file without folder path
    """
    path=os.getenv('DATA_PATH')+'json/'
    files = []
    for file in os.listdir(path):
        if filename in file:
            files.append(file)
    files.sort(reverse=True)
    return files[0]

#---------
#  Pickle
#---------
def dump_pickle(df,df_name):
    """
    Write a DataFrame to a PICKLE file in folder /data/pickle/ with the date
    """
    # Get todays date
    dt_string = datetime.now().strftime("%Y_%m_%d_%H%M")
    # Create folder if none existant
    path = os.getenv('DATA_PATH')+'pickle/'
    if not os.path.exists(path):
        os.makedirs(path)
    #Write the dataframe to pickle file
    df.to_pickle(path+df_name+'_'+dt_string+'.pkl')

def get_fresh_pickle(filename):
    """
    Return name of latest pickle file
    """
    path = os.getenv('DATA_PATH')+'pickle/'
    files = []
    r = "_\d{4}_\d{2}_\d{2}_\d{4}\.pkl"
    for file in os.listdir(path):
        if re.search(filename+r, file):
            files.append(file)
    files.sort(reverse=True)
    print(files[0])
    return path+files[0]

#-------
#  CSV
#-------
def get_fresh_csv(filename):
    """
    Return name of latest csv file
    """
    path = os.getenv('DATA_PATH')+'csv/'
    files = []
    r = "_\d{4}_\d{2}_\d{2}_\d{4}\.csv"
    for file in os.listdir(path):
        if re.search(filename+r, file):
            files.append(file)
    files.sort(reverse=True)
    print(files[0])
    return path+files[0]




