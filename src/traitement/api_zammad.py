
import os
from dotenv import load_dotenv
# loading environment variable :  path to data folder
load_dotenv()

import pandas as pd
from src.traitement.utils import read_simple_json, load_json_from_file, dump_pickle

def parse_zammad(filename='zammad', path=os.getenv('DATA_PATH')+'json/zammad/', as_file=True, as_frame=False):
    files = []
    for file in os.listdir(path):
        if filename in file:
            files.append(file)
    files.sort(reverse=False)

    # df = read_simple_json(load_json_from_file(files[0], path=path))

    pages = []
    for file in files:
        page = read_simple_json(load_json_from_file(file, path=path))
        pages.append(page)
    df = pd.concat(pages)
    # Generation
    if as_file==True:
        dump_pickle(df, filename)
    if as_frame==True:
        return df        
    print("Parsing Zammad completed.")
