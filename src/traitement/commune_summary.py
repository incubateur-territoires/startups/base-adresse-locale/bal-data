import pandas as pd
from src.traitement.utils import get_fresh_csv, dump_pickle


def parse_commune_summary(filename='commune_summary', as_file=True, as_frame=False):
    """
    
    """
    print(get_fresh_csv(filename))
    df = pd.read_csv(get_fresh_csv(filename))
    # df.rename(columns={'sync.status':'sync_status',
    #                    'sync.isPaused':'sync_isPaused',
    #                    'sync.currentUpdated':'sync_currentUpdated',
    #                    'sync.lastUploadedRevisionId':'sync_lastUploadedRevisionId' },
    #           inplace=True)
    # Generation
    if as_file==True:
        dump_pickle(df, filename)
    elif as_frame==True:
        return df        
    else : 
        print('nothing has been done.')
    print("Parsing commune_summary completed.")
    

