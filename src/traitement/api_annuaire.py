#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 27 17:47:31 2022

@author: panda
"""

from src.traitement.utils import read_simple_json, load_json_from_file, dump_pickle, get_fresh_json


def parse_annuaire(filename='annuaire', as_file=True, as_frame=False):
    df = read_simple_json(load_json_from_file(filename=get_fresh_json(filename))['features'][0])
    df = df[['properties.codeInsee','properties.email', 'properties.telephone','properties.nom'] ]
    df.rename(columns = {
        'properties.codeInsee':'code',
        'properties.email': 'mairie_email',
        'properties.telephone': 'mairie_telephone',
        'properties.nom':'mairie_nom'        
        },inplace=True)        
    # Generation
    if as_file==True:
        dump_pickle(df, filename)
    if as_frame==True:
        return df        
    print("Parsing Annuaire mairie completed.")

