#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 22 18:03:31 2022

@author: panda
"""

#-----------
#  Imports
#-----------
import os
from dotenv import load_dotenv
# loading environment variable :  path to data folder
load_dotenv()


import re
from datetime import datetime
import pandas as pd

from src import acquisition
from src.traitement.utils import get_fresh_pickle, dump_pickle, get_fresh_csv



#-------------
#   Functions
#-------------

    #------------------------------------
    #   Nettoyage/Selection des colonnes
    #------------------------------------


def clean_decoupage(df):    
    dic = {'codeDepartement':'code_departement',
            'codeRegion': 'code_region',
            'codesPostaux':'codes_postaux',
            'nomDepartement':'departement',
            'nomRegion':'region'
           }
    return df.rename(columns=dic)

def clean_densite(df):
    def padding(x):
        if len(x)==4:
            return '0'+str(x)
        else:
            return x
    df['Code Commune'] = df['Code Commune'].apply(padding)
    df.drop(['Libellé des communes',
             'Région',
             'Population municipale 2017'],
            axis=1, inplace=True)
    df.rename(columns={'Degré de Densité de la commune':'densite',
                       'Code Commune':'code'},
              inplace=True)

    return df

def clean_epci(df):
    """
    col name : codeEpci
    type : string
    descr : 

    col name : nomEpci
    type : string
    descr : 

    col name : typeEpci
    type : string
    values : CC CU CA ME CI
    descr : 

    """
    df.drop(['LIBGEO', 'DEP', 'REG'], axis=1, inplace=True)
    df.rename(columns={'EPCI'   : 'code_epci',
                        'LIBEPCI': 'epci',
                        'CODGEO' : 'code'},
               inplace=True)
    
    df['type_epci'] = pd.Series(dtype=object)
    for r, row in df.iterrows():
        if pd.isna(row.epci)==False:
            if re.match("^['C']{2}",row.epci):
                df.loc[r, 'type_epci']='CC'
            elif re.match("^['C']['U']",row.epci):
                df.loc[r, 'type_epci']='CU'
            elif re.match("^['C']['A']",row.epci):
                df.loc[r, 'type_epci']='CA'
            elif re.match(".*[mM]étropole.*",row.epci):
                df.loc[r, 'type_epci']='ME'
            else :
                df.loc[r, 'type_epci']='CI'
    
    return df


def clean_annuaire(df):
    # filtre
    exclusion = '|'.join(['déléguée','antenne'])
    df[df['mairie_nom'].str.contains(exclusion)==False]
    df = df.groupby('code').head(1).reset_index(drop=True)
    return df


def clean_commune_summary(df):
    """
    ['region', 'departement', 'code_commune', 'nom_commune', 'population',
       'type_composition', 'nb_lieux_dits', 'nb_voies', 'nb_numeros',
       'nb_numeros_certifies', 'analyse_adressage_nb_adresses_attendues',
       'analyse_adressage_ratio', 'analyse_adressage_deficit_adresses',
       'composed_at']
    """    
    df.drop(['region',
             'departement',
             'nom_commune','population'],
            axis=1, inplace=True)
    df.rename(columns={'code_commune':'code'}, inplace=True)
    return df


def clean_api_depot(df):
    """
           
    """
    def deduplicate(df):
        
        
        v = df.code.value_counts()
        # Les communes qui apparaissent une seule fois
        a = df[df.code.isin(v.index[v.eq(1)])]
        # Le dépot current = True des communes qui apparaissent plusieurs fois
        b= df[(df.code.isin(v.index[v.gt(1)]))&(df.current==True)]
        
        # Les communes restantes (à implémenter)
        
        return pd.concat([a,b])    
    
    df.rename(columns={'codeCommune':'code',
                          #'_id':'api_depot_id',
                        'status':'api_depot_status',
                        'createdAt':'api_depot_created',
                        'updatedAt':'api_depot_updated',
                        'publishedAt':'api_depot_published',
                        'client_nom':'api_depot_client_nom',
                        'client_mandataire':'api_depot_client_mandataire',
                        'habilitation_emailCommune':'api_depot_habilitation_email',
                        'habilitation_strategy_type':'api_depot_habilitation_strategy',
                        'validation_warnings':'api_depot_validation_warnings',
                        'validation_infos':'api_depot_validation_infos',
                        'client_organisme':'api_depot_client_organisme',
                      }, inplace=True)
    df = deduplicate(df)
    
    df.drop(['current', 'ready', 'context_originalSubmissionId',
            'context_extras_balId', 'validation_valid', 'validation_errors',        
            'context_nomComplet', 'context_organisation', 'habilitation_strategy_pinCode',
            'habilitation_strategy_pinCodeExpiration',
            'habilitation_strategy_remainingAttempts',
            'habilitation_strategy_createdAt',
            'habilitation__id',
            'habilitation_codeCommune', 	
            'habilitation_createdAt',
            'habilitation_updatedAt',
            'habilitation_expiresAt',
            'context_extras',
            'habilitation_strategy_mandat_nomMarital',
            'habilitation_strategy_mandat_nomNaissance',
            'habilitation_strategy_mandat_prenom', 	
            'habilitation_strategy_mandat_typeMandat', 
            'client_options_relaxMode',
            'client_chefDeFile',
            'context_extras_internal_id',
            'context_extras_user_id', 	
            'context_extras_env_id',
             '_id',
             'api_depot_status',
             'api_depot_created',
             'api_depot_updated',
             'client_id',
             'validation_validatorVersion',
             'validation_rowsCount'
             
            ],
        axis=1, inplace=True)
    
    
    return df



def clean_api_bal(df):
    """
    
    """
    df.rename(columns={'commune':'code',
                      '_id':'api_bal_id',
                      'status':'api_bal_status',
                      '_created':'api_bal_created',
                      '_updated':'api_bal_updated',
                      'nom':'api_bal_nom',
                     # 'nbNumeros':'api_bal_nb_numeros',
                      #'nbNumerosCertifies':'api_bal_nb_numeros_certifies', 
                      }, inplace=True)
    
    df.drop(['enableComplement',
             '_habilitation',
             'sync_status', 'sync_isPaused',
             'sync_currentUpdated',
             'sync_lastUploadedRevisionId',
             'nbNumeros',
             'nbNumerosCertifies'
            ],       
            axis=1, inplace=True)
    
    
    
    #adding a way to add a column (publised, pret à publier, draft, demo) for each commune.
    #>>>>> TO WORK ON
    
    # return only published BALs!! (mainly for deduplication
    df = df[df.api_bal_status=='published']
    
    return df 



def clean_api_backend(df):
    df.rename(columns={'commune_code':'code',
                      
                      }, inplace=True)

    df.drop(['commune_nom',
             'commune_rowsCount',
             'editeur_description',
             'editeur_page',
             'editeur_datagouv_nom',
             'editeur_datagouv_page',
             'editeur_datagouv_logo'
            ],       
            axis=1, inplace=True)
    
    return df



    #-----------------
    #   Consolidation
    #-----------------


def consolidation_densite(df, densite):
    
    df = df.merge(densite,how='outer', on='code')
    return df

def consolidation_epci(df,epci):
    df = df.merge(epci,how='outer', on='code')

    return df


def consolidation_annuaire(df, annuaire):
    df = df.merge(annuaire,how='left', on='code')
    return df


def consolidation_commune_summary(df, summary):
    df = df.merge(summary,how='left', on='code')
    return df




    #-----------
    #   Parsing
    #-----------



def parse_etat_deploiement_bal(as_file=True, as_frame=False, filename='etat_deploiement_bal'):
    
    # Load data into memory
    annuaire = pd.read_pickle(get_fresh_pickle('annuaire'))
    decoupage = pd.read_pickle(get_fresh_pickle('decoupage_administratif'))
    api_backend = pd.read_pickle(get_fresh_pickle('backend'))
    api_bal = pd.read_pickle(get_fresh_pickle('api_bal'))
    api_depot = pd.read_pickle(get_fresh_pickle('api_depot'))
    summary = pd.read_csv(get_fresh_csv('commune_summary'))
    # External sources
    epci = pd.read_csv(os.getenv('DATA_PATH')+'external/liste_epci_insee_2021.csv')
    densite = pd.read_csv(os.getenv('DATA_PATH')+'external/grille_densite_2020_agrege.csv')
    
    
    # Clean data
    decoupage = clean_decoupage(decoupage)
    densite = clean_densite(densite)
    epci = clean_epci(epci)
    annuaire = clean_annuaire(annuaire)
    summary = clean_commune_summary(summary)
    api_bal =  clean_api_bal(api_bal)
    api_backend = clean_api_backend(api_backend)
    api_depot = clean_api_depot(api_depot)

    # Consolidate around decoupage administratif
    etat_deploiement_bal = consolidation_densite(densite, decoupage) #ok
    etat_deploiement_bal = consolidation_epci(etat_deploiement_bal, epci) #ok
    etat_deploiement_bal = consolidation_annuaire(etat_deploiement_bal, annuaire)
    etat_deploiement_bal = consolidation_commune_summary(etat_deploiement_bal, summary)
    etat_deploiement_bal = etat_deploiement_bal.merge(api_bal,how='outer', on='code')
    etat_deploiement_bal = etat_deploiement_bal.merge(api_depot,how='outer', on='code')
    etat_deploiement_bal = etat_deploiement_bal.merge(api_backend,how='left', on='code')




    
    if as_file==True:
        dump_pickle(etat_deploiement_bal, filename)
    elif as_frame==True:
        return etat_deploiement_bal        
    else : 
        print('nothing has been done.')
    print("Parsing etat_deploiement_bal completed.")




