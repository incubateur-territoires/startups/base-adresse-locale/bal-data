#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 20 14:24:19 2022

@author: panda
"""
import os
from dotenv import load_dotenv
# loading environment variable :  path to data folder
load_dotenv()
import gzip
import pandas as pd

from src.traitement.utils import dump_pickle
    
#-----------------------
#         ban
#-----------------------
def parse_ban_csv(filename='ban_csv', date='latest', as_file=True, as_frame=False):
    """It is very memory intensive"""
    path = os.getenv('DATA_PATH')+'gz/ban/'+date+'/'
    files = []
    for file in os.listdir(path):
        if 'adresses' in file and not 'adresses-locales' in file :
            files.append(file)
    files.sort(reverse=False)
    with gzip.open(path+files[0]) as f:
        df = pd.read_csv(f, delimiter=';', low_memory=False)
    print(files)
    departements = list()
    for file in files[1:]:
        if 'adresses' in file and not 'adresses-locales' in file :
            try :
                print(file)
                with gzip.open(path+file) as f:
                    d = pd.read_csv(f, delimiter=';', low_memory=False)
                    if d.dropna().empty == False or d.shape[0]!=0 :
                        departements.append(d)
            except:
                print(f)
                print('NOT PARSED')
                
    df = pd.concat(departements)
    
    if as_file==True:
        dump_pickle(df, filename)
    elif as_frame==True:
        return df        
    else :
        print('nothing has been done.')
    print("Parsing Ban completed.")