# TODO Clean import and test from file (for now its copy paste from jupyter)

# ---------------
#     IMPORTS
# ---------------
import os
from dotenv import load_dotenv
# loading environment variable :  path to data folder
load_dotenv()

import numpy as np
import pandas as pd
from src.traitement.utils import load_json_from_file, get_fresh_json, dump_pickle


# -----------------------
#          backend
# -----------------------
def read_json_backend(js):  
    """
    Given a JSON file from API endpoints: backend.adresse.gouv.fr/datasets,
    return flattened DataFrame with each row a commune.
    Is used in recover_date_creation
    """
    # Load JSON into DataFrame object and flatten nested content
    meta_cols = ['id', 'title', 'description', 'page', 'model', 'license', 'rowsCount', 'errored', 'url', 'dateMAJ',
                 'isValid']
    df0 = pd.json_normalize(js, record_path=['communes'], record_prefix='commune_', meta=meta_cols,
                            meta_prefix='editeur_', errors='ignore')
    df1 = pd.json_normalize(js)[
        ['id', 'organization.name', 'organization.page', 'organization.logo', 'contour.type', 'contour.coordinates']]
    df = pd.merge(df0, df1, left_on=['editeur_id'], right_on=['id'])
    # 'id' is duplicate from 'editeur_id'
    df.drop('id', inplace=True, axis=1)
    return df


def fix_columns_backend(df): 
    """
    Load raw Bal Datasets DataFrame and return it with correct dtypes and columns names.
    Is used in recover_date_creation
    """
    # making old datasets (<2021) and new ones interoperable. Old ones dont have
    # commune_nom, commune_type, commune_rowsCount, commune_dateMAJ but only commune_0
    if 'commune_0' in df.columns:
        df.rename(columns={'commune_0': 'commune_code'}, inplace=True)
        df['commune_dateMAJ'] = df['editeur_dateMAJ']
        df['commune_type'] = pd.Series(dtype=str)
        df['commune_rowsCount'] = pd.Series(dtype=float)
    # Renaming columns
    df.rename(columns={'organization.name': 'editeur_datagouv_nom',
                       'organization.page': 'editeur_datagouv_page',
                       'organization.logo': 'editeur_datagouv_logo',
                       'contour.type': 'editeur_contour_type',
                       'contour.coordinates': 'editeur_contour_coordinates'},
              inplace=True)

    df['commune_dateMAJ'] = pd.to_datetime(df['commune_dateMAJ'], format='%Y-%m-%d')  # infer_datetime_format=True)
    df['editeur_dateMAJ'] = pd.to_datetime(df['editeur_dateMAJ'], format='%Y-%m-%d')  # infer_datetime_format=True)

    return df


def recover_date_creation_backend(df):
    """
    From the collection of bal JSON files (included histo-datasets.zip)
    Recover most ancient dateMAJ to assign as creation date
    """
    print('Recovering creation dates from archives... ', end='')

    # creating a list with all backend files
    list_json_datasets = []
    for json_file in os.listdir(os.getenv('DATA_PATH')+'external/balistique/api_backend_archives/'):
        if 'bal_' in json_file:
            list_json_datasets.append(json_file)
    list_json_datasets.sort(reverse=True)
    # Creating date_creation column
    df['commune_date_creation'] = pd.Series(dtype=object)
    # Converting to datetime objects relevant columns
    df['commune_date_creation'] = pd.to_datetime(df['commune_date_creation'], infer_datetime_format=True)

    for i in list_json_datasets:
        # loading backend file into dataframe
        old = fix_columns_backend(read_json_backend(
            load_json_from_file(filename=i, path=os.getenv('DATA_PATH')+'external/balistique/api_backend_archives/')))
        # droping communs without dates
        old.dropna(how='all', subset=['commune_dateMAJ', 'editeur_dateMAJ'], inplace=True)
        # sorting communes by date and removing duplicates (keeping only older)
        old = old.sort_values('commune_dateMAJ').drop_duplicates('commune_code', keep='first')
        # for each communes, look if it is present in older backends and has an earlier date
        for row in df.loc[df.commune_code.isin(old.commune_code)].itertuples():
            old_row = old.loc[old.commune_code == row.commune_code].iloc[0]
            earlier_date = pd.Series(
                [row.commune_dateMAJ, old_row.commune_dateMAJ, row.editeur_dateMAJ, old_row.editeur_dateMAJ]).min()
            df.loc[row[0], 'commune_date_creation'] = earlier_date
    
    
    
    print('Dates recovered')
    return df


def manual_recover_date_creation_backend(df):
    print('Recovering creation dates from manual listing... ', end='')
    dates_manuelles = pd.read_csv(os.getenv('DATA_PATH')+'external/liste_date_creation_manquantes.csv')
    dates_manuelles['date_iso'] = pd.to_datetime(dates_manuelles['date_iso'], format='%Y-%m-%d')
    for r, row in dates_manuelles.iterrows():
        df.loc[df.editeur_title == row.editeur, 'commune_date_creation'] = row.date_iso

    # Filling missing dates with commune_dateMAJ et editeur_dateMAJ
    df.loc[df.commune_date_creation.isna(), 'commune_date_creation'] = df.loc[df.commune_date_creation.isna(), 'commune_dateMAJ']
    df.loc[df.commune_date_creation.isna(), 'commune_date_creation'] = df.loc[df.commune_date_creation.isna(), 'editeur_dateMAJ']

    

    # Fixing impossible outlier TEMPORARY /!\ # Fixing impossible outlier TEMPORARY /!\# Fixing impossible outlier TEMPORARY /!\
    # df.loc[df.commune_nom=='Revest-Saint-Martin','commune_date_creation'] = pd.Timestamp('20210308') # Fixing impossible outlier TEMPORARY /!\
    df.loc[df.commune_nom == 'Huillé-Lézigné', 'commune_date_creation'] = pd.Timestamp('20220426')  # Fixing impossible outlier TEMPORARY /!\
    df.loc[df.commune_nom == 'Montigné-lès-Rairies', 'commune_date_creation'] = pd.Timestamp('20210426')  # Fixing impossible outlier TEMPORARY /!\
    
    # /!\ Glitch in backend data, removing outlier :
    df.loc[df.commune_code=='49209','commune_dateMAJ'] = np.nan        

    # TODO Fixing Belfort if necessary 5 aout 2019 BAL (Base Adresse Locale) du Grand Belfort V 1.3
    df.loc[df.editeur_id =='627da20d967593b208902161','commune_date_creation'] = pd.Timestamp('20190805')

    # IS A BAD IDEA BUT SIMPLE FIX TO MISSING DATES
    # Dates : filling missing dates with most ancient
    df.loc[ df[df.commune_date_creation.isna()].index , 'commune_date_creation'] = df.commune_date_creation.min()

    print('Dates recovered.')
    return df


def deduplicate_backend(df):
    # Dropping duplicates and keeping most recent ones
    # /!\ existing duplicates can be a valuable information to see how switched publishing tools!
    df = df.sort_values('commune_date_creation').drop_duplicates('commune_nom', keep='last')
    return df


def parse_api_backend(filename='backend', deduplicate=True, as_file=True, as_frame=False):
    print('Parsing backend..')
    df = fix_columns_backend(read_json_backend(load_json_from_file(filename=get_fresh_json(filename))))
    df = recover_date_creation_backend(df)
    df = manual_recover_date_creation_backend(df)
    if deduplicate == True:
        df = deduplicate_backend(df)

    # Generation
    if as_file==True:
        dump_pickle(df, filename)
    if as_frame==True:
        return df    
    print("Parsing backend completed.")
    
