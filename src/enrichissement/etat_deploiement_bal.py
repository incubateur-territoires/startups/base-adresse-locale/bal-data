#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul  8 13:39:15 2022

@author: panda
"""

import datetime
import json
import pandas as pd
import geopandas as gpd
from shapely import geometry

from src.traitement.utils import dump_pickle


def reindex_colums(df):
    '''
    Return etat_deploiement_bal with ordered colummns,
    so it can be use on a spreadsheet program.
    /!\ Colonnes present in source but missing from the list columns will be erased /!\
    '''
    columns = [
                'nom','code','codes_postaux','commune_type', 'epci','code_epci', 'type_epci','departement','code_departement','region',  'code_region',
                'mairie_nom','mairie_email', 'mairie_telephone', 
                'densite','population','surface','type_composition',
                'contour_type', 'contour','centre',
                'nb_numeros', 'nb_numeros_certifies', 'nb_lieux_dits', 'nb_voies', 
                'analyse_adressage_nb_adresses_attendues', 'analyse_adressage_ratio',
                'analyse_adressage_deficit_adresses',
                'composed_at', 
                'api_bal_id',
                'api_bal_nom', 'api_bal_status', 'api_bal_created', 'api_bal_updated',
                'api_depot_published', 'api_depot_client_nom',
                'api_depot_client_mandataire', 'api_depot_habilitation_email',
                'api_depot_habilitation_strategy', 'api_depot_validation_warnings',
                'api_depot_validation_infos', 'api_depot_client_organisme',
                'commune_dateMAJ','commune_date_creation', 'commune_ignored', 'editeur_id',
                'editeur_title', 'editeur_model', 'editeur_license',
                'editeur_rowsCount', 'editeur_errored', 'editeur_url',
                'editeur_dateMAJ', 'editeur_isValid', 'editeur_contour_type',
                'editeur_contour_coordinates'
                ]
    
    return df.reindex(columns=columns)


def add_use_mesadresses(df):
    '''
    Return True if a commune as a BAL in Mes Adresses,
    else, return False.
    '''
    #df.loc[ df[df.api_depot_client_nom=='Mes Adresses'].index, 'use_mesadresses'] = True
    df.loc[ df[df.api_bal_id.isna()==False].index, 'use_mesadresses'] = True
    df.loc[ df[df.use_mesadresses.isna()].index,'use_mesadresses']= False
    return df

def add_publish_bal(df):
    '''
    Return True if a commune publish a BAL in the BAN,
    else, return False.
    '''
    df.loc[ df[df.type_composition=='bal'].index, 'publish_bal'] = True
    df.loc[ df.publish_bal!=True,'publish_bal']= False
    df.loc[ df[(df.use_mesadresses)& ~(df.publish_bal)].index, 'publish_bal' ] = True
    return df

def fix_date(df):
    """
    # The new column 'publication_date' have biases:
    1 - the first update appearing in data is considered the creation date
    2 - Some outliers (1901, ovh incident, etc.) have been ironed out
    3 - Residuals communes, (communes without any temporal data) are assigned the earliest date for visualisation purposes.
    PS : date handling also happens in src>traitement>api_backend.py
    """
    
    # set columns to datetime and remove timezone (datetime64[ns, UTC] > datetime64[ns])
    df.api_bal_created = pd.to_datetime(df.api_bal_created, infer_datetime_format=True).dt.tz_localize(None)
    df.api_bal_updated = pd.to_datetime(df.api_bal_updated, infer_datetime_format=True).dt.tz_localize(None)
    df.api_depot_published = pd.to_datetime(df.api_depot_published, infer_datetime_format=True).dt.tz_localize(None)
    

    
    # getting the earliest date
    cols_time = [
            'api_depot_published',
            'api_bal_created',
            'api_bal_updated',
            'api_depot_published',
            'commune_dateMAJ',
            'editeur_dateMAJ',
            'commune_date_creation',
            ]
    df['publication_date'] = df[cols_time].min(axis=1)    
    # some ironing out of outliers, all dates are using datetime.datetime object.
    df.loc[df.nom == 'Nouméa', 'publication_date'] = datetime.datetime(2022,7,4,0,0,0)
    df.loc[df.nom == 'Pirae', 'publication_date'] = datetime.datetime(2022,7,1,0,0,0)
    
    # filling the empty with earliest date (for visualisation purposes onnly!)
    df.loc[df[(df.publication_date.isna())&(df.publish_bal==True)].index,'publication_date'] = df.publication_date.min()
    
    df.sort_values(['publication_date','nom'], inplace=True)
    return df


def add_api_bal_rank(df):
    """
    Assign a Rank to each Commune in order of publication date using Mes Adresses
    """
    #df['api_bal_rank'] = pd.Series(dtype=float)
    # Assign a rank   
    x = 1
    for row in df[df.use_mesadresses==True].sort_values('publication_date').itertuples(index=True):            
        df.at[row[0],'api_bal_rank'] = x
        x+=1                
    return df


def add_publication_rank(df, col_name='publication_rank'):
    """
    Assign a Rank to each Commune in order of BAL publication date
    """
    df[col_name] = pd.Series(dtype=float)
    x = 1
    for row in df.sort_values('publication_date').itertuples(index=True):
        if pd.notnull(row.publication_date):
            df.at[row[0],col_name] = x
            x+=1    
    return df

def add_publication_rank_ratio(df, col_name='publication_rank_ratio'):
    """
    Assign a % Rank to each Commune in order of BAL publication date
    """
    df[col_name] = pd.Series(dtype=float)
    x = 1
    tot=df.shape[0]
    for row in df.sort_values('publication_date').itertuples(index=True):
        if pd.notnull(row.publication_date):
            df.at[row[0],col_name] = (x*100)/tot
            x+=1    
    return df





def add_population_category(df):
    
    def category(x):
        if x <= 2000:
            return 'petite'
        elif x > 2000 and x <= 10_000:
            return 'moyenne'
        elif x > 10_000 and x <= 100_000:
            return 'grande'
        elif x >100_000: 
            return 'très_grande'

    df['population_category'] = df.population.apply(category)
    return df



def add_publication_rank_popcat(df):
    df['publication_rank_popcat'] = pd.Series(dtype=float)
    categories = ['petite', 'moyenne', 'grande', 'très_grande']
    for c in categories:
        frame =  df.loc[df.population_category==c].copy()

        x = 1
        for row in frame.sort_values('publication_date').itertuples(index=True):
            if pd.notnull(row.publication_date):
                frame.at[row[0],'publication_rank_popcat'] = x
                x+=1    
        df.loc[df.population_category==c,'publication_rank_popcat']=frame['publication_rank_popcat']
        
        
    return df


def add_publication_rank_popcat_ratio(df):
    df['publication_rank_popcat_ratio'] = pd.Series(dtype=float)
    categories = ['petite', 'moyenne', 'grande', 'très_grande']
    for c in categories:
        frame =  df.loc[df.population_category==c].copy()
        tot = frame.shape[0]
        x = 1
        for row in frame.sort_values('publication_date').itertuples(index=True):
            if pd.notnull(row.publication_date):
                frame.at[row[0],'publication_rank_popcat_ratio'] = x/tot*100
                x+=1    
        df.loc[df.population_category==c,'publication_rank_popcat_ratio']=frame['publication_rank_popcat_ratio']
    
    return df



def mainframe_to_geomainframe(df):
    def lat(x):
        if type(x)==list:
            return x[0]
        #return  literal_eval(x)[0]
    def long(x):
        if type(x)==list:
            return x[1]
        #return  literal_eval(x)[1]  

    def poly(x):
        if type(x)==list:
            return geometry.MultiLineString(x).convex_hull
        #return literal_eval(x)

    def multi_poly(x):
        #Converting the list of lists to a geojson string then to a shapely MultiPolygon
        dic = { "type": "MultiPolygon", "coordinates":[]}
        dic['coordinates'] = x
        g = [x.buffer(0) for x in geometry.shape(json.loads(str(dic).replace("'", '\"'))).buffer(0).geoms]
        return geometry.MultiPolygon(g)
    
    
    
    gdf = gpd.GeoDataFrame(df)
    gdf['contour_geo'] = gpd.GeoSeries()
    gdf['centre'] = gpd.points_from_xy(df.centre.apply(lat), df.centre.apply(long))
    for t in gdf[['contour','contour_type']].itertuples():
        if t.contour_type == 'Polygon':
            gdf.at[t[0],'contour_geo'] = poly(t.contour)
        elif t.contour_type == 'MultiPolygon':
            gdf.at[t[0],'contour_geo'] = multi_poly(t.contour)
    gdf['contour'] = gdf['contour_geo']
    gdf.drop(columns=['contour_geo'], inplace=True, axis=1)
    gdf.geometry = gdf.contour
    gdf.crs = {'proj': 'latlong', 'ellps': 'WGS84', 'datum': 'WGS84', 'no_defs': True}
    return gdf





def enhance_etat_deploiement_bal(df,as_frame=False, as_file=True):
    """
    Add a series of columns and corrections to etat_deploiement_bal.
    Function's order matters.
    """
    print('Starting enhancing..')
    df = reindex_colums(df)
    df = add_use_mesadresses(df)
    df = add_publish_bal(df)
    df = fix_date(df) # to finish
    df = add_api_bal_rank(df)
    df = add_publication_rank(df)
    df = add_publication_rank_ratio(df)
    df = add_population_category(df)
    df = add_publication_rank_popcat(df)
    df = add_publication_rank_popcat_ratio(df)
    df = mainframe_to_geomainframe(df)
    print('Enhancement done.')
    if as_file==True:
        dump_pickle(df,'etat_deploiement_bal')
    if as_frame==True:
        return df
