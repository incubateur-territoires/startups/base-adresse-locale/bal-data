#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 22 05:22:41 2021

@author: cactus
"""
#-----------
#  Imports
#-----------
import os
import sys
root_dir = os.path.join(os.getcwd(), '..')
sys.path.append(root_dir)



from src import acquisition

#---------------------
#   Functions
#---------------------
def download_data():
    print('Starting download...')
    acquisition.query_commune_summary()
    # acquisition.query_ban_csv()
    # acquisition.query_bal_csv()
    acquisition.query_zammad()
    acquisition.query_annuaire()
    acquisition.query_api_bal()
    # acquisition.query_api_backend()
    acquisition.query_api_depot()
    print('Finished downloading.')


#-----------
#   Main
#-----------



if __name__ == "__main__":
    print('Starting download...')
    acquisition.query_commune_summary()
    # acquisition.query_ban_csv()
    # acquisition.query_bal_csv()
    acquisition.query_zammad()
    acquisition.query_annuaire()
    acquisition.query_api_bal()
    # acquisition.query_api_backend()
    acquisition.query_api_depot()
    print('Finished downloading.')
