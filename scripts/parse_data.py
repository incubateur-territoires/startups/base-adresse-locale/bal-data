 #!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 27 16:23:58 2022

@author: panda
"""
#-----------
#  Imports
#-----------
import os
import sys
root_dir = os.path.join(os.getcwd(), '..')
sys.path.append(root_dir)

from src.traitement import api_backend, api_bal, api_depot, api_zammad, bal_csv
from src.traitement import api_annuaire, commune_summary, ban_csv
from src.traitement.etat_deploiement_bal import parse_etat_deploiement_bal 
from src.enrichissement.etat_deploiement_bal import enhance_etat_deploiement_bal

#---------------------
#   Functions
#---------------------
def parse_data():
    print('Starting parsing...')
    # api_backend.parse_api_backend()
    api_bal.parse_api_bal()
    api_depot.parse_api_depot()
    # api_zammad.parse_zammad()
    api_annuaire.parse_annuaire()
    commune_summary.parse_commune_summary()    
    # parse() return a frame with etat_deploiement_bal and enhance() add columns, fix stuff and dump a pickle
    enhance_etat_deploiement_bal(parse_etat_deploiement_bal(as_file=False, as_frame=True))
    
    # bal_csv.parse_bal_csv()
    # ban_csv.parse_ban_csv()
    
    print('Finished parsing.')
    


#-----------
#   Main
#-----------
if __name__ == "__main__":
    print('Starting parsing...')
    # api_backend.parse_api_backend()
    api_bal.parse_api_bal()
    api_depot.parse_api_depot()
    # api_zammad.parse_zammad()
    api_annuaire.parse_annuaire()
    commune_summary.parse_commune_summary()    
    # parse() return a frame with etat_deploiement_bal and enhance() add columns, fix stuff and dump a pickle
    enhance_etat_deploiement_bal(parse_etat_deploiement_bal(as_file=False, as_frame=True))
    
    # bal_csv.parse_bal_csv()
    # ban_csv.parse_ban_csv()
    
    print('Finished parsing.')

