#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 14 15:55:07 2022

@author: panda
"""

from parse_data import parse_data
from download_data import download_data

#-----------
#   Main
#-----------
if __name__ == "__main__":
    download_data()
    parse_data()
    